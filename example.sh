#!/bin/bash

example () {

	export USER=$(id -u) && \
	docker build -t weeb --build-arg USER=$USER . && \
	docker run -it --rm -v $(pwd)/anime:/usr/weeb/anime:z -u $USER weeb -d "Goblin Slayer" -o /usr/weeb/anime -r 1080

}
