
FROM python:alpine

#shit will fail if you don't set a user as a build-arg, using a uid
ARG USER=""

RUN apk add libxml2-dev libc-dev g++ libxslt-dev npm git

RUN mkdir -p /usr/weeb/anime

RUN adduser -Du $USER user

RUN chown -R $USER /usr/weeb/anime  

RUN pip install horrible-downloader 

RUN npm install webtorrent-cli -g

USER user

WORKDIR /usr/weeb/anime

ENTRYPOINT ["horrible-downloader"]
